
#  Teste Unirede Frontend



Projeto desenvolvido para o teste de desenvolvedor(a) da UNIREDE



##  Executando projeto

1.  Clonar o projeto

2.  Acessar a pasta do projeto

3.  Montando ambiente

	**Docker**

	`# docker-compose up`

	**Angular CLI**

	`# ng serve`

4.  Acessar url do projeto http://localhost:4200