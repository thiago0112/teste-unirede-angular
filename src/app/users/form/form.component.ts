import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { MatSnackBar } from "@angular/material";

import { User } from "../user";
import { Role } from "../role";
import { UserService } from "../user.service";
import { RoleService } from "../role.service";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"]
})
export class FormComponent implements OnInit {
  userForm = this.fb.group({
    login: [null, Validators.required],
    password: [null, Validators.required],
    role_id: [null, Validators.required]
  });

  /**
   * User id
   */
  id: number;

  /**
   * Roles list
   */
  roles: Role[];

  /**
   * Errors
   */
  error = {};

  /**
   * Page title
   */
  title = "Cadastrar Usuário";

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private roleService: RoleService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get("id"), 0);
    this.loadRoles();

    if (!this.id) {
      return;
    }

    this.loadUser();
    this.title = "Editar Usuário";
  }

  /**
   * Load user from id property
   */
  loadUser(): void {
    this.userService.get(this.id).subscribe(user => {
      Object.keys(user).forEach(key => {
        const control = this.userForm.get(key);
        const rawValue = user[key];
        const value =
          parseInt(rawValue, 0) == rawValue ? parseInt(rawValue, 0) : rawValue;

        if (control instanceof FormControl) {
          control.setValue(value);
        }
      });
    });
  }

  /**
   * Load roles list
   */
  loadRoles(): void {
    this.roleService.getList().subscribe(roles => (this.roles = roles));
  }

  /**
   * Save and redirect or show error messages
   */
  onSubmit(): void {
    if (!this.userForm.valid) {
      return;
    }

    this.save().subscribe(
      () => {
        this.snackBar.open(
          'Usuário salvo com sucesso',
          'Fechar',
          {duration: 2500}
        );

        this.router.navigate(['']);
      },
      resp => {
        this.snackBar.open(
          'Erro ao salvar usuário',
          'Fechar',
          { duration: 2500 }
        );

        this.error = resp.error;

        Object.keys(this.error).forEach(key => {
          const control = this.userForm.get(key);
          if (control instanceof FormControl) {
            control.setErrors({ incorrect: true });
          }
        });
      }
    );
  }

  /**
   * Call service to save data
   */
  private save(): Observable<any> {
    const user = this.userForm.value as User;
    if (Number.isNaN(this.id)) {
      return this.userService.create(user);
    } else {
      return this.userService.update(this.id, user);
    }
  }
}
