import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Role } from './role';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends ApiService {
  /**
   * Get list of roles
   * @return Observable<Role[]>
   */
  getList(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(`${this.apiURL}/roles`);
  }
}
