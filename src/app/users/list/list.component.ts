import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  dataSource: User[];
  displayedColumns = ['id', 'login', 'actions'];

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.loadUsers();
  }

  /**
   * Load users to table data source
   */
  loadUsers(): void {
    this.userService.getList().subscribe(users => this.dataSource = users);
  }

  /**
   * Delete user
   */
  delete(user: User): void {
    this.userService.delete(user).subscribe(() => {
      this.snackBar.open(
        'Usuário removido com sucesso',
        'Fechar',
        { duration: 2500 }
      );
      this.loadUsers();
    });
  }
}
