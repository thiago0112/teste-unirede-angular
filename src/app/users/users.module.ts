import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ListComponent } from './list/list.component';
import { UtilsModule } from '../utils/utils.module';
import { CustomMaterialModule } from '../custom-material.module';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [ListComponent, FormComponent],
  imports: [
    HttpClientModule,
    CommonModule,
    RouterModule,
    CustomMaterialModule,
    UtilsModule,
    ReactiveFormsModule
  ]
})
export class UsersModule {}
