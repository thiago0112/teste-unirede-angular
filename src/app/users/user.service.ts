import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { User } from "./user";
import { ApiService } from "../api.service";

@Injectable({
  providedIn: "root"
})
export class UserService extends ApiService {
  /**
   * GEt user
   * @param id user identifier
   * @return Observable<User>
   */
  get(id: number): Observable<User> {
    return this.httpClient.get<User>(`${this.apiURL}/users/${id}`);
  }

  /**
   * Get list of users
   * @return Observable<User[]>
   */
  getList(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.apiURL}/users`);
  }

  /**
   * Create User
   * @param user User
   * @return Observable<any>
   */
  create(user: User): Observable<any> {
    return this.httpClient.post<User[]>(`${this.apiURL}/users`, user);
  }

  /**
   * Update user
   * @param id number
   * @param user User
   * @return Observable<any>
   */
  update(id: number, user: User): Observable<any> {
    return this.httpClient.put<User[]>(`${this.apiURL}/users/${id}`, user);
  }

  /**
   * Remove user
   * @param user User
   * @return Observable<any>
   */
  delete(user: User): Observable<any> {
    return this.httpClient.delete(`${this.apiURL}/users/${user.id}`);
  }
}
