import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HeaderComponent } from './header/header.component';
import { CustomMaterialModule } from '../custom-material.module';
import { LoaderInterceptorService } from './loader-interceptor.service';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, RouterModule, CustomMaterialModule, NgxSpinnerModule],
  exports: [HeaderComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }
  ]
})
export class UtilsModule {}
